//Define the JSON request body
var jsonBody = {};
//Fill in the values into the jsonBody
jsonBody.username = restAuthUsername;
jsonBody.password = restAuthPassword;
jsonRequestBody = JSON.stringify(jsonBody);
//Create a new HTTP REST Request object for the REST host that was provided
var request = restHost.createRequest("POST", "/csp/gateway/am/api/login?access_token", jsonRequestBody);
request.contentType = "application/json";
request.setHeader("accept", "application/json");
 
//Attempt to execute the REST request
try {
    response = request.execute();
    jsonObject = JSON.parse(response.contentAsString);
    //Return back the authentication token and validity period using a property set
    try{
	    var tokenResponse = jsonObject.access_token;
	    System.debug("token: " + tokenResponse);
    } catch (ex) {
	throw ex + " No valid token";
    }
}
catch (e) {
    throw "There was an error executing the REST call:" + e;
}


//Define the JSON request body
var jsonBody = {
    'input': {
        'dName': dName
    }
};jsonObject
//jsonBody['input']['dName'] = dName;
jsonRequestBody = JSON.stringify(jsonBody);
System.log (jsonRequestBody)
//Create a new HTTP REST Request object for the REST host that was provided
var request = restHost.createRequest("POST", "/codestream/api/pipelines/" + pipelineId + "/executions", jsonRequestBody);
request.contentType = "application/json";
request.setHeader("accept", "application/json");
request.setHeader("Authorization", "Bearer " + tokenResponse);
 
//Attempt to execute the REST request
try {
    response = request.execute();
    System.log (response.statusCode);
    jsonObject = JSON.parse(response.contentAsString);
    UserMessage = "Пожалуйста, дождитесь письмо с уведомлением об успешном развертывании"
}
catch (e) {
    System.error("Error executing the REST operation: " + e);
}